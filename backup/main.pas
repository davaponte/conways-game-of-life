unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, ComCtrls, Grids, StdCtrls, Types;

type

  { TMainForm }

  TMainForm = class(TForm)
    btnNew : TButton;
    btnStart : TButton;
    btnStop : TButton;
    btnSave : TButton;
    btnOpen : TButton;
    OpenDlg : TOpenDialog;
    pGrid : TPanel;
    pControls : TPanel;
    SaveDlg : TSaveDialog;
    sbInfo : TStatusBar;
    sgGame : TStringGrid;
    tGame : TTimer;
    procedure btnNewClick(Sender : TObject);
    procedure btnOpenClick(Sender : TObject);
    procedure btnStartClick(Sender : TObject);
    procedure btnSaveClick(Sender : TObject);
    procedure btnStopClick(Sender : TObject);
    procedure FormCreate(Sender : TObject);
    procedure sgGameDrawCell(Sender : TObject; aCol, aRow : Integer; aRect : TRect; aState : TGridDrawState);
    procedure sgGameMouseDown(Sender : TObject; Button : TMouseButton; Shift : TShiftState; X, Y : Integer);
    procedure sgGameMouseMove(Sender : TObject; Shift : TShiftState; X, Y : Integer);
    procedure sgGameMouseUp(Sender : TObject; Button : TMouseButton; Shift : TShiftState; X, Y : Integer);
    procedure tGameTimer(Sender : TObject);
  private
    Iteration : integer;
    MousePressed : boolean;
  public

  end;

var
  MainForm : TMainForm;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.FormCreate(Sender : TObject);
begin
  sgGame.ColCount := 50;
  Width := (sgGame.ColCount * sgGame.DefaultColWidth) + pControls.Width + 4;
  sgGame.RowCount := 40;
  Height := (sgGame.RowCount * sgGame.DefaultRowHeight) + sbInfo.Height + 4;
  tGame.Enabled := False;
  btnNew.Click;
  sgGame.Cells[1, 0] := '1';
  sgGame.Cells[2, 1] := '1';
  sgGame.Cells[0, 2] := '1';
  sgGame.Cells[1, 2] := '1';
  sgGame.Cells[2, 2] := '1';
end;

procedure TMainForm.btnNewClick(Sender : TObject);
var
  r, c : Integer;
begin
  Iteration := 0;
  for c := 0 to sgGame.ColCount - 1 do
    for r := 0 to sgGame.RowCount - 1 do
      sgGame.Cells[c, r] := '0';

end;

procedure TMainForm.btnOpenClick(Sender : TObject);
begin
  if OpenDlg.Execute then
    sgGame.LoadFromFile(OpenDlg.FileName);
end;

procedure TMainForm.btnStartClick(Sender : TObject);
begin
  tGame.Enabled := True;
end;

procedure TMainForm.btnSaveClick(Sender : TObject);
var
  r, c : integer;
begin
  if SaveDlg.Execute then
    sgGame.SaveToFile(SaveDlg.FileName);
end;

procedure TMainForm.btnStopClick(Sender : TObject);
begin
  tGame.Enabled := False;
end;

procedure TMainForm.sgGameDrawCell(Sender : TObject; aCol, aRow : Integer; aRect : TRect; aState : TGridDrawState);
begin
  with TStringGrid(Sender) do begin
    if (Cells[aCol, aRow] = '0') then
      Canvas.Brush.Color := clWhite
    else
      Canvas.Brush.Color := clRed;

    Canvas.FillRect(aRect);
    Canvas.Brush.Color := clBlack;
  end;
end;

procedure TMainForm.sgGameMouseDown(Sender : TObject; Button : TMouseButton; Shift : TShiftState; X, Y : Integer);
var
  ACol, ARow : integer;
begin
  MousePressed := True;
  with TStringGrid(Sender) do begin
    MouseToCell(X, Y, ACol, ARow);
    if (Cells[ACol, ARow] = '0') then
      Cells[ACol, ARow] := '1'
    else
      Cells[ACol, ARow] := '0';
  end;

end;

procedure TMainForm.sgGameMouseMove(Sender : TObject; Shift : TShiftState; X, Y : Integer);
var
  ACol, ARow : integer;
begin
  if MousePressed then
    with TStringGrid(Sender) do begin
      MouseToCell(X, Y, ACol, ARow);
      Cells[ACol, ARow] := '1'
    end;
end;

procedure TMainForm.sgGameMouseUp(Sender : TObject; Button : TMouseButton; Shift : TShiftState; X, Y : Integer);
begin
  MousePressed := False;
end;

procedure TMainForm.tGameTimer(Sender : TObject);
var
  MaxRow, MaxCol : integer;

  function CountNeighbours(Col, Row : integer) : integer;
  var
    r, c : integer;
  begin
    Result := 0;
    for r := Row - 1 to Row + 1 do
      for c := Col - 1 to Col + 1 do begin
        if (r = Row) and (c = Col) then Continue; //Salta la misma celda. No la cuentes.
        if (r in [0..MaxRow]) and (c in [0..MaxCol]) and (sgGame.Cells[c, r] = '1') then
          Inc(Result);
      end;
  end;

var
  r, c : integer;
  NewState : array of array of char;
begin
  MaxRow := sgGame.RowCount;
  MaxCol := sgGame.ColCount;
  SetLength(NewState, MaxCol, MaxRow);
  Dec(MaxCol);
  Dec(MaxRow);
  for c := 0 to MaxCol do
    for r := 0 to MaxRow do begin
      NewState[c, r] := '0'; //Preset
    //Regla 1: Una celda viva con menos de dos vecinas muere por aislamiento
    //Regla 2: Una celda viva con dos o tres vecinas pasa a la siguiente generación
    //Regla 3: Una celda viva con más de tres vecinas muere por sobrepoblación
      if (sgGame.Cells[c, r] = '1') and (CountNeighbours(c, r) in [2, 3]) then
        NewState[c, r] := '1';
//      else
//        NewState[c, r] := '0';
    //Regla 4: Una celda vacía con exactamente tres vecinas cobra vida
      if (sgGame.Cells[c, r] = '0') and (CountNeighbours(c, r) = 3) then
        NewState[c, r] := '1';

    end;
//Iteración completa terminada. Hay que copiar el NewState
  for c := 0 to MaxCol do
    for r := 0 to MaxRow do
      sgGame.Cells[c, r] := NewState[c, r];

  Inc(Iteration);
  sbInfo.SimpleText := 'Iteration: ' + IntToStr(Iteration);
  Application.ProcessMessages;
end;

end.

